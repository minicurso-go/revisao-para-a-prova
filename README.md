<h1>Revisão para a ultima prova de logica dificil s2</h1>
<ol>
    <li>Crie uma struct chamada Triângulo com os campos "base" e "altura". Escreva uma função que receba um Triângulo como parâmetro e calcule a área do triângulo (área = base * altura / 2).</li>
    <li>Crie uma struct chamada Funcionário com os campos "nome", "salário" e "idade". Escreva funções que permitam aumentar ou diminuir o salário do funcionário em uma determinada porcentagem e uma função que calcule o tempo de serviço do funcionário (considerando que ele começou a trabalhar aos 18 anos).</li>
    <li>Crie uma struct chamada Animal com os campos "nome", "espécie", "idade" e "som". Escreva funções que permitam modificar o som que o animal faz e uma função que imprima as informações do animal e o som que ele faz.</li>
    <li>Crie uma struct chamada Viagem com os campos "origem", "destino", "data" e "preço". Escreva uma função que receba um slice de Viagens como parâmetro e retorne a viagem mais cara.</li>
    <li>Crie uma struct chamada Aluno com os campos "nome", "idade" e "notas". O campo "notas" deve ser um slice de float64, representando as notas que o aluno tirou em uma determinada disciplina. Escreva funções que permitam adicionar ou remover notas do aluno, calcular a média das notas e imprimir o nome, idade e média do aluno.</li>
    <li>Crie um Array bidimensional de inteiros com 3 linhas e 2 colunas. Solicite ao usuário que informe os valores de cada elemento da matriz. Em seguida, imprima a matriz resultante.</li>
    <li>Crie um Array bidimensional de inteiros com 2 linhas e 3 colunas. Em seguida, solicite ao usuário que informe um índice de linha e outro de coluna e imprima o valor armazenado nessa posição da matriz.</li>
    <li>Escreva uma função que conte a ocorrência de cada palavra em uma string e retorne um mapa onde as chaves são as palavras encontradas e os valores são o número de ocorrências de cada palavra.</li>
    <li>Escreva uma função que receba dois mapas e retorne um novo mapa contendo todos os elementos dos mapas de entrada. Em caso de chaves duplicadas, o valor do segundo mapa deve prevalecer.</li>
    <li>Escreva uma função que receba um mapa com valores inteiros e retorne a soma de todos os valores.</li>
    <li>Escreva uma função que receba uma lista de mapas, onde cada mapa contém a contagem de palavras de um texto, e retorne um único mapa contendo a soma de todas as contagens.</li>
    <li>Escreva uma função que receba uma string contendo uma frase e retorne um mapa onde as chaves são as palavras encontradas na frase e os valores são mapas contendo a contagem de cada letra em cada palavra.</li>
    <li>Escreva uma função que receba um slice de inteiros como parâmetro e retorne um novo slice com apenas os números pares contidos no slice. Caso o slice esteja vazio, retorne um erro.</li>
    <li>Escreva uma função que receba um slice de inteiros como parâmetro e retorne um novo slice com os valores ordenados de forma crescente. Caso o slice esteja vazio, retorne um erro.</li>
    <li>Crie uma função que receba um número inteiro como parâmetro e retorne a soma de todos os seus dígitos. Caso o número seja negativo, retorne um erro.</li>
    <li>Crie uma função que receba um número inteiro e um slice de inteiros como parâmetros e retorne verdadeiro se o número estiver presente no slice e falso caso contrário. Caso o slice esteja vazio, retorne um erro.</li>
    <li>Crie uma função que receba um número inteiro como parâmetro e retorne um novo slice contendo todos os números primos menores ou iguais a ele. Caso o número seja negativo, retorne um erro.</li>
    <li>Escreva uma função que receba um slice de strings como parâmetro e retorne um novo slice contendo apenas as strings que possuem mais de 5 caracteres. Caso o slice esteja vazio, retorne um erro.</li>
</ol>